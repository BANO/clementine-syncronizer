extern crate clap;
extern crate dirs;
extern crate rusqlite;
extern crate terminal_size;
extern crate url;
extern crate walkdir;

use std::fs;
use std::io;
use std::io::Write;
use std::path::PathBuf;

use track::Track;

mod config;
mod track;
mod db;
mod walker;
mod progress;


fn main() {
    let config = config::get();

    println!("Opening db: {}", config.db.path.to_str().unwrap());

    let conn = db::open(config.db.path.to_path_buf())
        .expect("Could not open db");

    let ids = conn.fetch_track_ids_from_playlist(config.db.playlist.to_owned());

    let mut tracks: Vec<Track> = Vec::new();
    for id in ids {
        if let Ok(track) = Track::from_db(&conn.conn, id) {
            tracks.push(track)
        }
    }
    println!("Tracks in db found: {}", tracks.len());

    println!("Using dir: {}", config.dest.path.to_str().unwrap());
    let cur_tracks = walker::get_tracks(config.dest.path.to_path_buf());
    println!("Current tracks found: {}", cur_tracks.len());

    let cur_track_ids: Vec<u32> = cur_tracks
        .iter()
        .map(|a| a.id)
        .collect();
    let track_ids: Vec<u32> = tracks
        .iter()
        .map(|a| a.id)
        .collect();

//    Coping files

    let to_add: Vec<u32> = track_ids.to_owned().into_iter()
        .filter(|id| !cur_track_ids.to_owned().iter().any(|a| *a == *id))
        .collect();
    let to_add_len = to_add.len() as i32;

    if to_add_len > 0 {
        println!("Files to copy: {}", to_add_len);

        let mut bar = progress::Bar::new();

        bar.set_job_title("Coping files");

        let to_add_tracks = tracks.into_iter()
            .filter(|track| to_add.iter().any(|a| *a == track.id));

        let mut i = 0;
        for track in to_add_tracks {
            let full_name = new_path_from_track(&track, &config);

            bar.set_job_title(&format!("cp: {}", &track));

            fs::copy(&track.filename, full_name)
                .expect("Failed to copy file");


//            print log about copy
            io::stdout().flush().unwrap();

            let log = format!(" [+] {}", &track);
            print!("\r{}", &log);
            for _ in 0..(bar.get_terminal_size() - log.chars().count() as i32) {
                print!(" ")
            }
            println!();


            i += 1;
            bar.reach_percent(i * 100 / to_add_len);
        }
        bar.jobs_done();
    }

//    Removing files

    let to_remove: Vec<u32> = cur_track_ids.into_iter()
        .filter(|id| !track_ids.iter().any(|a| *a == *id))
        .collect();
    let to_remove_len = to_remove.len() as i32;

    if to_remove_len > 0 {
        println!("Files to remove: {}", to_remove_len);

        let mut bar = progress::Bar::new();

        bar.set_job_title("Removing files");

        let to_remove_tracks = cur_tracks.into_iter()
            .filter(|track| to_remove.iter().any(|a| *a == track.id));

        let mut i = 0;
        for track in to_remove_tracks {
            bar.set_job_title(&format!(" [-] {:?}", (&track.filename.file_name())));

            fs::remove_file(&track.filename)
                .expect("Failed to remove file");


//          print log about remove
            io::stdout().flush().unwrap();

            let log = format!("rm: {}", &track);
            print!("\r{}", &log);
            for _ in 0..(bar.get_terminal_size() - log.chars().count() as i32) {
                print!(" ")
            }
            println!();


            i += 1;
            bar.reach_percent(i * 100 / to_remove_len);
        }
        bar.jobs_done();
    }
}

fn new_path_from_track(track: &Track, config: &config::Config) -> PathBuf {
    let track_path = &track.filename;
    let new_name = track.filename_from_template(&config.dest.filename_template.to_string());

    let mut a = config.dest.path.to_path_buf();
    a.push(new_name);

    return a;
}