use std::ffi::OsStr;
use std::i64;
use std::path::{Path, PathBuf};
use track::Track;
use walkdir::{DirEntry, WalkDir};

pub fn get_tracks(path: PathBuf) -> Vec<Track> {
    get_files(path)
        .iter()
        .map(|e| file_into_track(e))
        .filter(|e| e.is_some())
        .map(|e| e.unwrap())
        .collect()
}

fn get_files(path: PathBuf) -> Vec<DirEntry> {
    WalkDir::new(path).into_iter()
        .map(|e| e.unwrap())
        .filter(|e| e.file_type().is_file())
        .collect()
}

fn file_into_track(entry: &DirEntry) -> Option<Track> {
    let path = Path::new(entry.file_name());
    let id_hex = Path::new(path.file_stem().unwrap()).extension();


    id_hex
        .or(Some(OsStr::new("")))
        .map(|id|
            match i64::from_str_radix(id.to_str().unwrap(), 16) {
                Ok(t) => Some(t as u32),
                Err(e) => None
            }
        )
        .unwrap()
        .map(|id|
            Track {
                id,
                filename: PathBuf::from(entry.path()),
                album: String::new(),
                title: String::new(),
                artist: String::new(),
            }
        )
}