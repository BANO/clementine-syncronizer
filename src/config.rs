use clap::{App, Arg};
use dirs;
use std::{fs, path};
use std::path::PathBuf;


pub struct Config {
    pub db: DbConfig,
    pub dest: Destconfig,
}

pub struct DbConfig {
    pub path: PathBuf,
    pub playlist: String,
}

pub struct Destconfig {
    pub filename_template: String,
    pub path: PathBuf,
}

impl Config {
    fn new(db: PathBuf, template: String, dest: PathBuf, playlist: String) -> Config {
        Config {
            db: DbConfig {
                path: db,
                playlist,
            },
            dest: Destconfig {
                path: dest,
                filename_template: template,
            },
        }
    }
}

pub fn get() -> Config {
    let matches = App::new("Clementine syncronizer")
        .version("0.1.0-alpha")
        .author("BANO.notIT <bano.notit@gmail.com>")
        .about("Does awesome things")
        .arg(Arg::with_name("dest")
            .short("w")
            .long("dest")
            .value_name("Path")
            .help("Sets a custom config file")
            .takes_value(true)
            .default_value("./")
        )
        .arg(Arg::with_name("playlist")
            .short("p")
            .long("playlist")
            .value_name("Name")
            .help("Playlist name to fetch tracks from")
            .takes_value(true)
        )
        .arg(Arg::with_name("database")
            .short("d")
            .long("db")
            .value_name("Clementine DB path")
            .takes_value(true)
            .default_value("~/.config/Clementine/clementine.db")
        )
        .arg(Arg::with_name("filename")
            .short("t")
            .long("template")
            .value_name("Template")
            .takes_value(true)
            .default_value("%artist - %title")
        )
        .get_matches();


    let cwd = matches.value_of("dest")
        .map(|path| {
            fs::canonicalize(path)
                .expect("Could not get current dir")
        })
        .unwrap();
    let db_path = {
        let mut raw_db_path: String = String::from(
            matches.value_of("database")
                .expect("Could not get db path")
        );
        if raw_db_path.starts_with('~') {
            raw_db_path =
                raw_db_path.replace(
                    '~',
                    dirs::home_dir()
                        .expect("Could not get home dir")
                        .to_str()
                        .unwrap(),
                )
        }
        raw_db_path
    };
    let db_path = path::PathBuf::from(db_path).canonicalize()
        .expect("Could not find that file");


    Config::new(
        db_path,
        matches.value_of("filename").unwrap().to_string(),
        cwd,
        matches.value_of("playlist").unwrap().to_string(),
    )
}